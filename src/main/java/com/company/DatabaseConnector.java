package com.company;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class DatabaseConnector {
  private static final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
//  private static final String DATABASE_URL = "jdbc:mysql://localhost:3306/openmrs_cccrn2?useUnicode=true&serverTimezone=UTC";
//  private static final String USERNAME = "root";
//  private static final String PASSWORD = "bright1008";

  private static Connection DATABASE_CONNECTION;

  public DatabaseConnector() {
  }

  public static Connection getConnection() throws IOException {
    Properties prop = new Properties();
    prop.load(new FileInputStream("C://sqlite/data.properties"));
    String USERNAME = prop.getProperty("username");
    String PASSWORD = prop.getProperty("password");
    String DATABASE_URL = prop.getProperty("database_url");
    try {
      Class.forName(JDBC_DRIVER);
      if (DATABASE_CONNECTION == null || DATABASE_CONNECTION.isClosed()) {
        try {
          DATABASE_CONNECTION = DriverManager.getConnection(
              DATABASE_URL,
              USERNAME,
              PASSWORD
          );
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return DATABASE_CONNECTION;
  }
}
