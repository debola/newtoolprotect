package com.company;

import com.company.model.entity.Progress;
import com.company.model.entity.Routine;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

public class PostRequestExt {

    public void post(List<Routine>  records) throws IOException, InterruptedException {
        Properties prop = new Properties();
        prop.load(new FileInputStream("C://sqlite/data.properties"));
        String api_url_1 = prop.getProperty("api_url_1");
        String token = prop.getProperty("token");
        List<HashMap<String, String>> recordMapsList = new ArrayList<>();
        if (!records.isEmpty()) {
            for (Routine routine : records) {

                HashMap values = new HashMap<String, String>() {
                    {
                        put("facility_name", routine.getFacility_name());
                        put("datim_code", routine.getDatim_code());
                        put("sex", routine.getSex());
                        put("dob", routine.getDob());
                        put("enrolment_date", routine.getEnrolment_date());
                        put("weight_at_enrolment", routine.getWeight_at_enrolment());
                        put("who_stage_at_enrolment", routine.getWho_stage_at_enrolment());
                        put("symptoms", routine.getSymptoms());
                        put("cd4_count_at_hiv_diagnosis", routine.getCd4_count_at_hiv_diagnosis());
                        put("date_of_first_cd4", routine.getDate_of_first_cd4());
                        put("tb_status_at_enrolment", routine.getTb_status_at_enrolment());
                        put("arv_regimen_at_art_start", routine.getArv_regimen_at_art_start());
                        put("current_arv_regimen", routine.getCurrent_arv_regimen());
                        put("art_start_date", routine.getArt_start_date());
                        put("oi_drugs_medication", routine.getOi_drugs_medication());
                        put("arv_medication", routine.getArv_medication());
                        put("anti_tb_medication", routine.getAnti_tb_medication());
                        put("other_drugs_prescribed", routine.getOther_drugs_prescribed());
                        put("prior_art", routine.getPrior_art());
                        put("current_weight", routine.getCurrent_weight());
                        put("current_weight_date", routine.getCurrent_weight_date());
                        put("reasons_for_stopping_arvs", routine.getReasons_for_stopping_arvs());
                        put("tpt_start_date", routine.getTpt_start_date());
                        put("tpt_completion_date", routine.getTpt_completion_date());
                        put("current_tb_status", routine.getCurrent_tb_status());
                        put("cd4_count_current", routine.getCd4_count_current());
                        put("date_of_current_cd4", routine.getDate_of_current_cd4());
                        put("current_viral_Load", routine.getCurrent_viral_Load());
                        put("date_of_current_viral_load", routine.getDate_of_current_viral_load());
                        put("tb_treatment_completed", routine.getTb_treatment_completed());
                        put("current_height", routine.getCurrent_height());
                        put("current_temperature", routine.getCurrent_temperature());
                        put("current_systolic", routine.getCurrent_systolic());
                        put("current_diastolic", routine.getCurrent_diastolic());
                        put("current_bmi", routine.getCurrent_bmi());
                        put("current_muac_child", routine.getCurrent_muac_child());
                        put("current_muac_bmi", routine.getCurrent_muac_bmi());
                        put("functional_status", routine.getFunctional_status());
                        put("current_who_hiv_stage", routine.getCurrent_who_hiv_stage());
                    }
                };
                recordMapsList.add(values);
            }

            ObjectMapper objectMapper = new ObjectMapper();
            String requestBody = objectMapper
                    .writeValueAsString(recordMapsList);

            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(api_url_1))
                    .header("Content-Type", "application/json")
                    .header("Authorization", "Bearer " + token)
                    .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                    .build();

            HttpResponse<String> response = client.send(request,
                    HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == 200 || response.statusCode() == 201) {
                new SqliteConnect().delete(records.get(0).getDatim_code());
            }
        }
    }

    public void postProgress(List<Progress>  records) throws IOException, InterruptedException {
        Properties prop = new Properties();
        prop.load(new FileInputStream("data.properties"));
        String api_url_2 = prop.getProperty("api_url_2");
        String token = prop.getProperty("token");
        List<HashMap<String, String>> recordMapsList = new ArrayList<>();
        if (!records.isEmpty()) {
            for (Progress routine : records) {
                HashMap values = new HashMap<String, String>() {
                    {
                        put("facility_name", routine.getFacility_name());
                        put("datim_code", routine.getDatim_code());
                        put("sex", routine.getSex());
                        put("dob", routine.getDob());
                        put("enrolment_date", routine.getEnrolment_date());
                        put("creator", routine.getCreator());
                        put("date_created", routine.getDate_created());
                        put("type_of_patient", routine.getType_of_patient());
                        put("previously_known_hiv_status", routine.getPreviously_known_hiv_status());
                        put("end_of_treatment", routine.getEnd_of_treatment());
                        put("previously_known_hiv_status_presumtive", routine.getPreviously_known_hiv_status_presumtive());
                        put("tested_for_hiv", routine.getTested_for_hiv());
                        put("type_of_care", routine.getType_of_care());
                        put("hiv_test_result", routine.getHiv_test_result());
                        put("art_start_date", routine.getArt_start_date());
                        put("treatment_regimen", routine.getTreatment_regimen());
                        put("date_registered", routine.getDate_registered());
                        put("date_treatment_started", routine.getDate_treatment_started());
                        put("treatment_outcome_date", routine.getTreatment_outcome_date());
                        put("treatment_outcome", routine.getTreatment_outcome());
                        put("tb_screening_score", routine.getTb_screening_score());
                        put("hiv_status", routine.getHiv_status());
                        put("on_art", routine.getOn_art());
                        put("outcome", routine.getOutcome());
                        put("type_of_treatment_regimen", routine.getType_of_treatment_regimen());
                        put("registration_group", routine.getRegistration_group());
                        put("outcome_date", routine.getOutcome_date());
                        put("form_id", routine.getForm_id());
                    }
                };
                recordMapsList.add(values);
            }
            ObjectMapper objectMapper = new ObjectMapper();
            String requestBody = objectMapper
                    .writeValueAsString(recordMapsList);

            HttpClient client = HttpClient.newHttpClient();
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(URI.create(api_url_2))
                    .header("Content-Type", "application/json")
                    .header("Authorization", "Bearer " + token)
                    .POST(HttpRequest.BodyPublishers.ofString(requestBody))
                    .build();

            HttpResponse<String> response = client.send(request,
                    HttpResponse.BodyHandlers.ofString());

            if (response.statusCode() == 200 || response.statusCode() == 201) {
                new SqliteConnect().deleteProgress(records.get(0).getDatim_code());
            }
        }
    }
}