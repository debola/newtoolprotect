package com.company;

import com.company.model.entity.Progress;
import com.company.model.entity.Routine;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SqliteConnect {

    public void createNewTable() {
        // SQL statement for creating a new table
        String sql = "CREATE TABLE IF NOT EXISTS tbanalysis (\n"
                + "  facility_name text NOT NULL,\n"
                + "  datim_code text NOT NULL,\n"
                + "  sex text  NULL,\n"
                + "  dob text  NULL,\n"
                + "  enrolment_date text  NULL,\n"
                + "  weight_at_enrolment text  NULL,\n"
                + "  who_stage_at_enrolment text  NULL,\n"
                + "  symptoms text  NULL,\n"
                + "  cd4_count_at_hiv_diagnosis text  NULL,\n"
                + "  date_of_first_cd4 text  NULL,\n"
                + "  tb_status_at_enrolment text  NULL,\n"
                + "  arv_regimen_at_art_start text  NULL,\n"
                + "  current_arv_regimen text  NULL,\n"
                + "  art_start_date text  NULL,\n"
                + "  oi_drugs_medication text  NULL,\n"
                + "  arv_medication text  NULL,\n"
                + "  anti_tb_medication text  NULL,\n"
                + "  other_drugs_prescribed text  NULL,\n"
                + "  prior_art text  NULL,\n"
                + "  current_weight text  NULL,\n"
                + "  current_weight_date text  NULL,\n"
                + "  reasons_for_stopping_arvs text  NULL,\n"
                + "  tpt_start_date text  NULL,\n"
                + "  tpt_completion_date text  NULL,\n"
                + "  current_tb_status text  NULL,\n"
                + "  cd4_count_current text  NULL,\n"
                + "  date_of_current_cd4 text  NULL,\n"
                + "  current_viral_Load text  NULL,\n"
                + "  date_of_current_viral_load text  NULL,\n"
                + "  tb_treatment_completed text  NULL,\n"
                + "  current_height text  NULL,\n"
                + "  current_temperature text  NULL,\n"
                + "  current_systolic text  NULL,\n"
                + "  current_diastolic text  NULL,\n"
                + "  current_bmi text  NULL,\n"
                + "  current_muac_child text  NULL,\n"
                + "  current_muac_bmi text NULL,\n"
                + "  functional_status text  NULL,\n"
                + "  current_who_hiv_stage text  NULL\n"
                + ");";

        String sql_progress = "CREATE TABLE IF NOT EXISTS tbprogress (\n"
                + "  facility_name text NOT NULL,\n"
                + "  datim_code text NOT NULL,\n"
                + "  sex text  NULL,\n"
                + "  dob text  NULL,\n"
                + "  creator text  NULL,\n"
                + "  date_created text  NULL,\n"
                + "  enrolment_date text  NULL,\n"
                + "  type_of_patient text  NULL,\n"
                + "  previously_known_hiv_status text  NULL,\n"
                + "  end_of_treatment text  NULL,\n"
                + "  previously_known_hiv_status_presumtive text  NULL,\n"
                + "  tested_for_hiv text  NULL,\n"
                + "  type_of_care text  NULL,\n"
                + "  hiv_test_result text  NULL,\n"
                + "  treatment_regimen text  NULL,\n"
                + "  date_registered text  NULL,\n"
                + "  date_treatment_started text  NULL,\n"
                + "  treatment_outcome_date text  NULL,\n"
                + "  treatment_outcome text  NULL,\n"
                + "  tb_screening_score text  NULL,\n"
                + "  hiv_status text  NULL,\n"
                + "  on_art text  NULL,\n"
                + "  outcome text  NULL,\n"
                + "  type_of_treatment_regimen text  NULL,\n"
                + "  registration_group text  NULL,\n"
                + "  art_start_date text  NULL,\n"
                + "  outcome_date text  NULL,\n"
                + "  form_id text  NULL\n"
                + ");";

        try (Connection conn = this.connect();
             Statement stmt = conn.createStatement()) {
            // create a new table
            stmt.execute(sql);
            stmt.execute(sql_progress);
            System.out.println("A new table has been created.");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private Connection connect() {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        Connection conn = null;
        try {
            String url = "jdbc:sqlite:C://sqlite/db/project.db";
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    public List<Routine> selectAll(){
        String sql = "SELECT * FROM tbanalysis";
        List<Routine> records = new ArrayList<>();

        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet resultSet  = stmt.executeQuery(sql)){
            // loop through the result set
            while (resultSet.next()) {
                Routine record = new Routine(
                        resultSet.getString("facility_name"),
                        resultSet.getString("datim_code"),
                        resultSet.getString("sex"),
                        resultSet.getString("dob"),
                        resultSet.getString("enrolment_date"),
                        resultSet.getString("weight_at_enrolment"),
                        resultSet.getString("who_stage_at_enrolment"),
                        resultSet.getString("symptoms"),
                        resultSet.getString("cd4_count_at_hiv_diagnosis"),
                        resultSet.getString("date_of_first_cd4"),
                        resultSet.getString("tb_status_at_enrolment"),
                        resultSet.getString("arv_regimen_at_art_start"),
                        resultSet.getString("current_arv_regimen"),
                        resultSet.getString("art_start_date"),
                        resultSet.getString("oi_drugs_medication"),
                        resultSet.getString("arv_medication"),
                        resultSet.getString("anti_tb_medication"),
                        resultSet.getString("other_drugs_prescribed"),
                        resultSet.getString("prior_art"),
                        resultSet.getString("current_weight"),
                        resultSet.getString("current_weight_date"),
                        resultSet.getString("reasons_for_stopping_arvs"),
                        resultSet.getString("tpt_start_date"),
                        resultSet.getString("tpt_completion_date"),
                        resultSet.getString("current_tb_status"),
                        resultSet.getString("cd4_count_current"),
                        resultSet.getString("date_of_current_cd4"),
                        resultSet.getString("current_viral_Load"),
                        resultSet.getString("date_of_current_viral_load"),
                        resultSet.getString("tb_treatment_completed"),
                        resultSet.getString("current_height"),
                        resultSet.getString("current_temperature"),
                        resultSet.getString("current_systolic"),
                        resultSet.getString("current_diastolic"),
                        resultSet.getString("current_bmi"),
                        resultSet.getString("current_muac_child"),
                        resultSet.getString("current_muac_bmi"),
                        resultSet.getString("functional_status"),
                        resultSet.getString("current_who_hiv_stage")
                );
                records.add(record);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return records;
    }

    public void delete(String id) {
        String sql = "DELETE FROM tbanalysis WHERE datim_code = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, id);
            // execute the delete statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void createNewDatabase(String fileName) {

        String url = "jdbc:sqlite:C:/sqlite/db/" + fileName;

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("A new database has been created.");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void insert(List<Routine> records) {
        String sql = "INSERT INTO tbanalysis(facility_name, datim_code, sex, dob, enrolment_date, weight_at_enrolment, who_stage_at_enrolment, symptoms, cd4_count_at_hiv_diagnosis, date_of_first_cd4, tb_status_at_enrolment, arv_regimen_at_art_start, current_arv_regimen, art_start_date, oi_drugs_medication, arv_medication, anti_tb_medication, other_drugs_prescribed, prior_art, current_weight, current_weight_date, reasons_for_stopping_arvs, tpt_start_date, tpt_completion_date, current_tb_status, cd4_count_current, date_of_current_cd4, current_viral_Load, date_of_current_viral_load, tb_treatment_completed, current_height, current_temperature, current_systolic, current_diastolic, current_bmi, current_muac_child, current_muac_bmi, functional_status, current_who_hiv_stage ) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            for (Routine routine : records) {
                pstmt.setString(1, routine.getFacility_name());
                pstmt.setString(2, routine.getDatim_code());
                pstmt.setString(3, routine.getSex());
                pstmt.setString(4, routine.getDob());
                pstmt.setString(5, routine.getEnrolment_date());
                pstmt.setString(6, routine.getWeight_at_enrolment());
                pstmt.setString(7, routine.getWho_stage_at_enrolment());
                pstmt.setString(8, routine.getSymptoms());
                pstmt.setString(9, routine.getCd4_count_at_hiv_diagnosis());
                pstmt.setString(10, routine.getDate_of_first_cd4());
                pstmt.setString(11, routine.getTb_status_at_enrolment());
                pstmt.setString(12, routine.getArv_regimen_at_art_start());
                pstmt.setString(13, routine.getCurrent_arv_regimen());
                pstmt.setString(14, routine.getArt_start_date());
                pstmt.setString(15, routine.getOi_drugs_medication());
                pstmt.setString(16, routine.getArv_medication());
                pstmt.setString(17, routine.getAnti_tb_medication());
                pstmt.setString(18, routine.getOther_drugs_prescribed());
                pstmt.setString(19, routine.getPrior_art());
                pstmt.setString(20, routine.getCurrent_weight());
                pstmt.setString(21, routine.getCurrent_weight_date());
                pstmt.setString(22, routine.getReasons_for_stopping_arvs());
                pstmt.setString(23, routine.getTpt_start_date());
                pstmt.setString(24, routine.getTpt_completion_date());
                pstmt.setString(25, routine.getCurrent_tb_status());
                pstmt.setString(26, routine.getCd4_count_current());
                pstmt.setString(27, routine.getDate_of_current_cd4());
                pstmt.setString(28, routine.getCurrent_viral_Load());
                pstmt.setString(29, routine.getDate_of_current_viral_load());
                pstmt.setString(30, routine.getTb_treatment_completed());
                pstmt.setString(31, routine.getCurrent_height());
                pstmt.setString(32, routine.getCurrent_temperature());
                pstmt.setString(33, routine.getCurrent_systolic());
                pstmt.setString(34, routine.getCurrent_diastolic());
                pstmt.setString(35, routine.getCurrent_bmi());
                pstmt.setString(36, routine.getCurrent_muac_child());
                pstmt.setString(37, routine.getCurrent_muac_bmi());
                pstmt.setString(38, routine.getFunctional_status());
                pstmt.setString(39, routine.getCurrent_who_hiv_stage());
                pstmt.executeUpdate();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

//
    public List<Progress> selectAllProgress(){
        String sql = "SELECT * FROM tbprogress";
        List<Progress> records = new ArrayList<>();

        try (Connection conn = this.connect();
             Statement stmt  = conn.createStatement();
             ResultSet resultSet    = stmt.executeQuery(sql)){
            // loop through the result set
            while (resultSet.next()) {
                Progress record = new Progress(
                        resultSet.getString("facility_name"),
                        resultSet.getString("datim_code"),
                        resultSet.getString("sex"),
                        resultSet.getString("dob"),
                        resultSet.getString("creator"),
                        resultSet.getString("date_created"),
                        resultSet.getString("enrolment_date"),
                        resultSet.getString("type_of_patient"),
                        resultSet.getString("previously_known_hiv_status"),
                        resultSet.getString("end_of_treatment"),
                        resultSet.getString("previously_known_hiv_status_presumtive"),
                        resultSet.getString("tested_for_hiv"),
                        resultSet.getString("type_of_care"),
                        resultSet.getString("hiv_test_result"),
                        resultSet.getString("treatment_regimen"),
                        resultSet.getString("date_registered"),
                        resultSet.getString("date_treatment_started"),
                        resultSet.getString("treatment_outcome_date"),
                        resultSet.getString("treatment_outcome"),
                        resultSet.getString("tb_screening_score"),
                        resultSet.getString("hiv_status"),
                        resultSet.getString("on_art"),
                        resultSet.getString("outcome"),
                        resultSet.getString("type_of_treatment_regimen"),
                        resultSet.getString("registration_group"),
                        resultSet.getString("art_start_date"),
                        resultSet.getString("outcome_date"),
                        resultSet.getString("form_id")
                );
                records.add(record);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return records;
    }

    public void deleteProgress(String id) {
        String sql = "DELETE FROM tbprogress WHERE datim_code = ?";

        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            // set the corresponding param
            pstmt.setString(1, id);
            // execute the delete statement
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void insertProgress(List<Progress> records) {
        String sql = "INSERT INTO tbprogress(facility_name, datim_code, sex, dob, creator, date_created, enrolment_date, type_of_patient, previously_known_hiv_status, end_of_treatment, previously_known_hiv_status_presumtive, tested_for_hiv, type_of_care, hiv_test_result, treatment_regimen, date_registered, date_treatment_started, treatment_outcome_date, treatment_outcome, tb_screening_score, hiv_status, on_art, outcome, type_of_treatment_regimen, registration_group, art_start_date, outcome_date, form_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try (Connection conn = this.connect();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            for (Progress routine : records) {
                pstmt.setString(1, routine.getFacility_name());
                pstmt.setString(2, routine.getDatim_code());
                pstmt.setString(3, routine.getSex());
                pstmt.setString(4, routine.getDob());
                pstmt.setString(5, routine.getCreator());
                pstmt.setString(6, routine.getDate_created());
                pstmt.setString(7, routine.getEnrolment_date());
                pstmt.setString(8, routine.getType_of_patient());
                pstmt.setString(9, routine.getPreviously_known_hiv_status());
                pstmt.setString(10, routine.getEnd_of_treatment());
                pstmt.setString(11, routine.getPreviously_known_hiv_status_presumtive());
                pstmt.setString(12, routine.getTested_for_hiv());
                pstmt.setString(13, routine.getType_of_care());
                pstmt.setString(14, routine.getHiv_test_result());
                pstmt.setString(15, routine.getTreatment_regimen());
                pstmt.setString(16, routine.getDate_registered());
                pstmt.setString(17, routine.getDate_treatment_started());
                pstmt.setString(18, routine.getTreatment_outcome_date());
                pstmt.setString(19, routine.getTreatment_outcome());
                pstmt.setString(20, routine.getTb_screening_score());
                pstmt.setString(21, routine.getHiv_status());
                pstmt.setString(22, routine.getOn_art());
                pstmt.setString(23, routine.getOutcome());
                pstmt.setString(24, routine.getType_of_treatment_regimen());
                pstmt.setString(25, routine.getRegistration_group());
                pstmt.setString(26, routine.getArt_start_date());
                pstmt.setString(27, routine.getOutcome_date());
                pstmt.setString(27, routine.getForm_id());
                pstmt.executeUpdate();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
