package com.company.controller;

import java.sql.SQLException;
import java.util.List;


public interface GenericController<E> {

  List<E> findAll() throws SQLException;

}
