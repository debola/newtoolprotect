package com.company.controller;

import com.company.model.dao.implementation.ProgressDAO;
import com.company.model.dao.implementation.RoutineDAO;
import com.company.model.entity.Progress;
import com.company.model.entity.Routine;

import java.sql.SQLException;
import java.util.List;


public class ProgressController implements GenericController<Progress>  {
    ProgressDAO dao = new ProgressDAO();

    @Override
    public List<Progress> findAll() throws SQLException {
        return dao.findAll();
    }

}
