package com.company.controller;

import com.company.model.dao.implementation.RoutineDAO;
import com.company.model.entity.Progress;
import com.company.model.entity.Routine;

import java.sql.SQLException;
import java.util.List;


public class RoutineController implements GenericController<Routine>  {
    RoutineDAO dao = new RoutineDAO();

    @Override
    public List<Routine> findAll() throws SQLException {
        return dao.findAll();
    }

}
