package com.company.model.dao;

import java.sql.SQLException;
import java.util.List;


public interface GenericDAO<E> {

  List<E> findAll() throws SQLException;
}
