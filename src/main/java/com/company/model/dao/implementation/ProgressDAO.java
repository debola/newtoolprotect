package com.company.model.dao.implementation;

        import com.company.DatabaseConnector;
        import com.company.model.dao.GenericDAO;
        import com.company.model.entity.Progress;
        import com.company.model.entity.Routine;

        import java.sql.PreparedStatement;
        import java.sql.ResultSet;
        import java.util.ArrayList;
        import java.util.List;


public class ProgressDAO implements GenericDAO<Progress> {

    private static final String GET_ALL_PROGRESS = "SELECT \n" +
            "  (\n" +
            "    SELECT \n" +
            "      property_value \n" +
            "    FROM \n" +
            "      global_property \n" +
            "    WHERE \n" +
            "      property = 'Facility_Name'\n" +
            "  ) AS facility_name, \n" +
            "  (\n" +
            "    SELECT \n" +
            "      property_value \n" +
            "    FROM \n" +
            "      global_property \n" +
            "    WHERE \n" +
            "      property = 'Facility_Datim_Code'\n" +
            "  ) AS datim_code, \n" +
            "  pe.gender AS sex, \n" +
            "  pe.birthdate AS dob, \n" +
            "  usr.username AS creator, \n" +
            "  (\n" +
            "    DATE_FORMAT(\n" +
            "      e.date_created, '%Y-%m-%d'\n" +
            "    )\n" +
            "  ) AS date_created, \n" +
            "\n" +
            "  MIN(\n" +
            "    DATE_FORMAT(\n" +
            "      e.encounter_datetime, '%Y-%m-%d'\n" +
            "    )\n" +
            "  ) AS enrolment_date, \n" +
            "  (\n" +
            "    IF(\n" +
            "      obs.concept_id = 166687, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS type_of_patient, \n" +
            "  (\n" +
            "    IF(\n" +
            "      obs.concept_id = 166030, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS 'previously_known_hiv_status', \n" +
            "  (\n" +
            "    IF(\n" +
            "      obs.concept_id = 166030, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS 'end_of_treatment', \n" +
            "  (\n" +
            "    IF(\n" +
            "      obs.concept_id = 166798, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS 'previously_known_hiv_status_presumtive', \n" +
            "  (\n" +
            "    IF(\n" +
            "      obs.concept_id = 164401, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS 'tested_for_hiv', \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 165351, \n" +
            "      cn.name, \n" +
            "      IF(\n" +
            "        obs.concept_id = 160119, \n" +
            "        cn.name, \n" +
            "        IF(\n" +
            "          obs.concept_id = 160434, cn.name, NULL\n" +
            "        )\n" +
            "      )\n" +
            "    )\n" +
            "  ) AS 'type_of_care', \n" +
            "  (\n" +
            "    IF(\n" +
            "      obs.concept_id = 159427, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS 'hiv_test_result', \n" +
            "  (\n" +
            "    IF(\n" +
            "      obs.concept_id = 166804, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS 'treatment_regimen', \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 166686, \n" +
            "      DATE_FORMAT(obs.obs_datetime, '%d-%b-%Y'), \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS date_registered, \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 166692, \n" +
            "      DATE_FORMAT(obs.obs_datetime, '%d-%b-%Y'), \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS date_treatment_started, \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 166675, \n" +
            "      DATE_FORMAT(obs.obs_datetime, '%d-%b-%Y'), \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS treatment_outcome_date, \n" +
            "  (\n" +
            "    IF(\n" +
            "      obs.concept_id = 159786, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS 'treatment_outcome', \n" +
            "  (\n" +
            "    IF(\n" +
            "      obs.concept_id = 165808, obs.value_numeric, \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS 'tb_screening_score', \n" +
            "  (\n" +
            "    IF(\n" +
            "      obs.concept_id = 159576, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS 'hiv_status', \n" +
            "  (\n" +
            "    IF(\n" +
            "      obs.concept_id = 159811, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS 'on_art', \n" +
            "  (\n" +
            "    IF(\n" +
            "      obs.concept_id = 159786, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS 'outcome', \n" +
            "  (\n" +
            "    IF(\n" +
            "      obs.concept_id = 166540, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS 'type_of_treatment_regimen', \n" +
            "  (\n" +
            "    IF(\n" +
            "      obs.concept_id = 159990, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS 'registration_group', \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 159599, \n" +
            "      DATE_FORMAT(obs.obs_datetime, '%d-%b-%Y'), \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS art_start_date, \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 159787, \n" +
            "      DATE_FORMAT(obs.obs_datetime, '%d-%b-%Y'), \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS outcome_date,\n" +
            "  e.form_id\n" +
            "FROM \n" +
            "  patient pa \n" +
            "  LEFT JOIN person pe ON pa.patient_id = pe.person_id \n" +
            "  LEFT JOIN person_name pn ON pa.patient_id = pn.person_id \n" +
            "  LEFT JOIN patient_identifier pid ON pa.patient_id = pid.patient_id \n" +
            "  LEFT JOIN encounter e ON e.patient_id = pa.patient_id \n" +
            "  LEFT JOIN users usr ON e.creator = usr.user_id\n" +
            "  LEFT JOIN obs ON obs.encounter_id = e.encounter_id \n" +
            "  LEFT JOIN concept_name cn ON(\n" +
            "    cn.concept_id = obs.value_coded \n" +
            "    AND cn.locale = 'en'\n" +
            "  ) \n" +
            "  \n" +
            "WHERE \n" +
            "  pa.voided = 0 \n" +
            "  AND e.form_id IN (91,92,96,90,102,85,86,87,88,93,95,98,100,101,103,104,105,106)\n" +
            "  AND pid.identifier_type = 4 -- AND obs.encounter_id = e.encounter_id\n" +
            "  AND obs.voided = 0 -- and e.encounter_datetime between @startdate and @enddate\n" +
            "GROUP BY \n" +
            "  pid.identifier;\n";



    @Override
    public List<Progress> findAll() {
        List<Progress> records = new ArrayList<>();
        try (PreparedStatement statement = DatabaseConnector.getConnection().prepareStatement(GET_ALL_PROGRESS)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Progress record = new Progress(
                        resultSet.getString("facility_name"),
                        resultSet.getString("datim_code"),
                        resultSet.getString("sex"),
                        resultSet.getString("dob"),
                        resultSet.getString("creator"),
                        resultSet.getString("date_created"),
                        resultSet.getString("enrolment_date"),
                        resultSet.getString("type_of_patient"),
                        resultSet.getString("previously_known_hiv_status"),
                        resultSet.getString("end_of_treatment"),
                        resultSet.getString("previously_known_hiv_status_presumtive"),
                        resultSet.getString("tested_for_hiv"),
                        resultSet.getString("type_of_care"),
                        resultSet.getString("hiv_test_result"),
                        resultSet.getString("treatment_regimen"),
                        resultSet.getString("date_registered"),
                        resultSet.getString("date_treatment_started"),
                        resultSet.getString("treatment_outcome_date"),
                        resultSet.getString("treatment_outcome"),
                        resultSet.getString("tb_screening_score"),
                        resultSet.getString("hiv_status"),
                        resultSet.getString("on_art"),
                        resultSet.getString("outcome"),
                        resultSet.getString("type_of_treatment_regimen"),
                        resultSet.getString("registration_group"),
                        resultSet.getString("art_start_date"),
                        resultSet.getString("outcome_date"),
                        resultSet.getString("form_id")
                );
                records.add(record);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return records;
    }

}
