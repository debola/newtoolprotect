package com.company.model.dao.implementation;

import com.company.DatabaseConnector;
import com.company.model.dao.GenericDAO;
import com.company.model.entity.Progress;
import com.company.model.entity.Routine;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


public class RoutineDAO implements GenericDAO<Routine> {

    private static final String GET_ALL = "SELECT \n" +
            "  (\n" +
            "    SELECT \n" +
            "      property_value \n" +
            "    FROM \n" +
            "      global_property \n" +
            "    WHERE \n" +
            "      property = 'Facility_Name'\n" +
            "  ) AS facility_name, \n" +
            "  (\n" +
            "    SELECT \n" +
            "      property_value \n" +
            "    FROM \n" +
            "      global_property \n" +
            "    WHERE \n" +
            "      property = 'Facility_Datim_Code'\n" +
            "  ) AS datim_code, \n" +
            "  pe.gender AS sex, \n" +
            "  pe.birthdate AS dob, \n" +
            "  MIN(\n" +
            "    DATE_FORMAT(\n" +
            "      e.encounter_datetime, '%Y-%m-%d'\n" +
            "    )\n" +
            "  ) AS enrolment_date, \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 165582, obs.value_numeric, \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS weight_at_enrolment, \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 5356, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS 'who_stage_at_enrolment', \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 1727, cn.name, IF(\n" +
            "      obs.concept_id = 160170, cn.name, NULL\n" +
            "    )\n" +
            "    )\n" +
            "  ) AS 'symptoms', \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 164429, \n" +
            "      obs.value_numeric, \n" +
            "      IF(\n" +
            "        obs.concept_id = 167088, \n" +
            "        IF(\n" +
            "          obs.value_coded = 167086, '<200', '>=200'\n" +
            "        ), \n" +
            "        IF(\n" +
            "          obs.concept_id = 5497, obs.value_numeric, \n" +
            "          NULL\n" +
            "        )\n" +
            "      )\n" +
            "    )\n" +
            "  ) AS cd4_count_at_hiv_diagnosis, \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 164429, \n" +
            "      DATE_FORMAT(obs.obs_datetime, '%Y-%m-%d'), \n" +
            "      IF(\n" +
            "        obs.concept_id = 5497, \n" +
            "        DATE_FORMAT(obs.obs_datetime, '%Y-%m-%d'), \n" +
            "        IF(\n" +
            "          obs.concept_id = 167088, \n" +
            "          DATE_FORMAT(obs.obs_datetime, '%Y-%m-%d'), \n" +
            "          NULL\n" +
            "        )\n" +
            "      )\n" +
            "    )\n" +
            "  ) AS date_of_first_cd4, \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 1659, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS tb_status_at_enrolment, \n" +
            "   (\n" +
            "    SELECT \n" +
            "      cn.NAME \n" +
            "    FROM \n" +
            "      obs ob \n" +
            "      JOIN concept_name cn ON cn.concept_id = ob.value_coded \n" +
            "      JOIN encounter e ON ob.encounter_id = e.encounter_id \n" +
            "    WHERE \n" +
            "      ob.concept_id IN (\n" +
            "        164506, 164513, 165702, 164507, 164514, \n" +
            "        165703\n" +
            "      ) \n" +
            "      AND cn.locale = 'en' \n" +
            "      AND cn.locale_preferred = 1 \n" +
            "      AND ob.person_id = pa.patient_id \n" +
            "      AND e.encounter_type = 13 \n" +
            "      AND ob.voided = 0 \n" +
            "      AND e.voided = 0 \n" +
            "    ORDER BY \n" +
            "      ob.obs_datetime ASC \n" +
            "    LIMIT \n" +
            "      1\n" +
            "  ) AS arv_regimen_at_art_start, \n" +
            "  (\n" +
            "    SELECT \n" +
            "      cn.NAME \n" +
            "    FROM \n" +
            "      obs ob \n" +
            "      JOIN concept_name cn ON cn.concept_id = ob.value_coded \n" +
            "      JOIN encounter e ON ob.encounter_id = e.encounter_id \n" +
            "    WHERE \n" +
            "      ob.concept_id IN (\n" +
            "        164506, 164513, 165702, 164507, 164514, \n" +
            "        165703\n" +
            "      ) \n" +
            "      AND cn.locale = 'en' \n" +
            "      AND cn.locale_preferred = 1 \n" +
            "      AND ob.person_id = pa.patient_id \n" +
            "      AND e.encounter_type = 13 \n" +
            "      AND ob.voided = 0 \n" +
            "      AND e.voided = 0 \n" +
            "    ORDER BY \n" +
            "      ob.obs_datetime DESC \n" +
            "    LIMIT \n" +
            "      1\n" +
            "  ) AS current_arv_regimen, \n" +
            "  \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 159599, \n" +
            "      DATE_FORMAT(obs.value_datetime, '%Y-%m-%d'), \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS art_start_date, \n" +
            "  (\n" +
            "    SELECT \n" +
            "      cn.NAME \n" +
            "    FROM \n" +
            "      obs ob \n" +
            "      JOIN concept_name cn ON cn.concept_id = ob.value_coded \n" +
            "      JOIN encounter e ON ob.encounter_id = e.encounter_id \n" +
            "    WHERE \n" +
            "      ob.concept_id = 165727\n" +
            "      AND cn.locale = 'en' \n" +
            "      AND cn.locale_preferred = 1 \n" +
            "      AND ob.person_id = pa.patient_id \n" +
            "      AND e.encounter_type = 13 \n" +
            "      AND ob.voided = 0 \n" +
            "      AND e.voided = 0 \n" +
            "    ORDER BY \n" +
            "      ob.obs_datetime DESC \n" +
            "    LIMIT \n" +
            "      1\n" +
            "  ) AS oi_drugs_medication, \n" +
            "  (\n" +
            "    SELECT \n" +
            "      cn.NAME \n" +
            "    FROM \n" +
            "      obs ob \n" +
            "      JOIN concept_name cn ON cn.concept_id = ob.value_coded \n" +
            "      JOIN encounter e ON ob.encounter_id = e.encounter_id \n" +
            "    WHERE \n" +
            "      ob.concept_id = 165724\n" +
            "      AND cn.locale = 'en' \n" +
            "      AND cn.locale_preferred = 1 \n" +
            "      AND ob.person_id = pa.patient_id \n" +
            "      AND e.encounter_type = 13 \n" +
            "      AND ob.voided = 0 \n" +
            "      AND e.voided = 0 \n" +
            "    ORDER BY \n" +
            "      ob.obs_datetime DESC \n" +
            "    LIMIT \n" +
            "      1\n" +
            "  ) AS arv_medication, \n" +
            "  (\n" +
            "    SELECT \n" +
            "      cn.NAME \n" +
            "    FROM \n" +
            "      obs ob \n" +
            "      JOIN concept_name cn ON cn.concept_id = ob.value_coded \n" +
            "      JOIN encounter e ON ob.encounter_id = e.encounter_id \n" +
            "    WHERE \n" +
            "      ob.concept_id = 165304\n" +
            "      AND cn.locale = 'en' \n" +
            "      AND cn.locale_preferred = 1 \n" +
            "      AND ob.person_id = pa.patient_id \n" +
            "      AND e.encounter_type = 13 \n" +
            "      AND ob.voided = 0 \n" +
            "      AND e.voided = 0 \n" +
            "    ORDER BY \n" +
            "      ob.obs_datetime DESC \n" +
            "    LIMIT \n" +
            "      1\n" +
            "  ) AS anti_tb_medication,   \n" +
            "  (IF(\n" +
            "    obs.concept_id = 163101, obs.value_text, \n" +
            "    NULL\n" +
            "  )) AS other_drugs_prescribed,\n" +
            "  \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 165766, \n" +
            "      IF(obs.value_coded = 1, \"Yes\", \"No\"), \n" +
            "      IF(\n" +
            "        obs.concept_id = 165242, cn.name, NULL\n" +
            "      )\n" +
            "    )\n" +
            "  ) AS prior_art, \n" +
            "  MIN(\n" +
            "    IF(\n" +
            "      obs.concept_id = 165470, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS reasons_for_stopping_arvs, \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 5089, obs.value_numeric, \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS current_weight, \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 5089, \n" +
            "      DATE_FORMAT(obs.obs_datetime, '%Y-%m-%d'), \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS current_weight_date, \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 165994, \n" +
            "      DATE_FORMAT(obs.obs_datetime, '%Y-%m-%d'), \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS tpt_start_date, \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 166008, \n" +
            "      DATE_FORMAT(obs.obs_datetime, '%Y-%m-%d'), \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS tpt_completion_date, \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 1659, cn.name, NULL\n" +
            "    )\n" +
            "  ) AS current_tb_status, \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 164429, \n" +
            "      obs.value_numeric, \n" +
            "      IF(\n" +
            "        obs.concept_id = 167088, \n" +
            "        IF(\n" +
            "          obs.value_coded = 167086, '<200', '>=200'\n" +
            "        ), \n" +
            "        IF(\n" +
            "          obs.concept_id = 5497, obs.value_numeric, \n" +
            "          NULL\n" +
            "        )\n" +
            "      )\n" +
            "    )\n" +
            "  ) AS cd4_count_current, \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 164429, \n" +
            "      DATE_FORMAT(obs.obs_datetime, '%Y-%m-%d'), \n" +
            "      IF(\n" +
            "        obs.concept_id = 5497, \n" +
            "        DATE_FORMAT(obs.obs_datetime, '%Y-%m-%d'), \n" +
            "        IF(\n" +
            "          obs.concept_id = 167088, \n" +
            "          DATE_FORMAT(obs.obs_datetime, '%Y-%m-%d'), \n" +
            "          NULL\n" +
            "        )\n" +
            "      )\n" +
            "    )\n" +
            "  ) AS date_of_current_cd4, \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 856, obs.value_numeric, \n" +
            "      IF(\n" +
            "      obs.concept_id = 166422, obs.value_numeric, \n" +
            "      NULL\n" +
            "    )\n" +
            "    )\n" +
            "  ) AS current_viral_Load, \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 856, \n" +
            "      DATE_FORMAT(obs.obs_datetime, '%Y-%m-%d'), \n" +
            "      IF(\n" +
            "      obs.concept_id = 166422, \n" +
            "      DATE_FORMAT(obs.obs_datetime, '%Y-%m-%d'), \n" +
            "      NULL\n" +
            "    )\n" +
            "    )\n" +
            "  ) AS date_of_current_viral_load, \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 166675, \n" +
            "      DATE_FORMAT(obs.obs_datetime, '%Y-%m-%d'), \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS tb_treatment_completed,\n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 5090, obs.value_numeric, \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS current_height, \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 5088, obs.value_numeric, \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS current_temperature, \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 5085, obs.value_numeric, \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS current_systolic, \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 5086, obs.value_numeric, \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS current_diastolic,  \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 1342, obs.value_numeric, \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS current_bmi,  \n" +
            "   MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 165935, cn.name, \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS current_muac_child,  \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 165243, cn.name, \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS current_muac_bmi,   \n" +
            "    \n" +
            "   MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 165039, cn.name, \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS functional_status,     \n" +
            "  MAX(\n" +
            "    IF(\n" +
            "      obs.concept_id = 5356, cn.name, \n" +
            "      NULL\n" +
            "    )\n" +
            "  ) AS current_who_hiv_stage\n" +
            "\n" +
            "FROM \n" +
            "  patient pa \n" +
            "  LEFT JOIN person pe ON pa.patient_id = pe.person_id \n" +
            "  LEFT JOIN person_name pn ON pa.patient_id = pn.person_id \n" +
            "  LEFT JOIN patient_identifier pid ON pa.patient_id = pid.patient_id \n" +
            "  LEFT JOIN encounter e ON e.patient_id = pa.patient_id \n" +
            "  LEFT JOIN obs ON obs.encounter_id = e.encounter_id \n" +
            "  LEFT JOIN concept_name cn ON(\n" +
            "    cn.concept_id = obs.value_coded \n" +
            "    AND cn.locale = 'en'\n" +
            "  ) \n" +
            "WHERE \n" +
            "  pa.voided = 0 \n" +
            "  AND pid.identifier_type = 4 -- AND obs.encounter_id = e.encounter_id\n" +
            "  \n" +
            "  AND obs.voided = 0 -- and e.encounter_datetime between @startdate and @enddate\n" +
            "GROUP BY \n" +
            "  pid.identifier;\n";


    @Override
    public List<Routine> findAll() {
        List<Routine> records = new ArrayList<>();
        try (PreparedStatement statement = DatabaseConnector.getConnection().prepareStatement(GET_ALL)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Routine record = new Routine(
                        resultSet.getString("facility_name"),
                        resultSet.getString("datim_code"),
                        resultSet.getString("sex"),
                        resultSet.getString("dob"),
                        resultSet.getString("enrolment_date"),
                        resultSet.getString("weight_at_enrolment"),
                        resultSet.getString("who_stage_at_enrolment"),
                        resultSet.getString("symptoms"),
                        resultSet.getString("cd4_count_at_hiv_diagnosis"),
                        resultSet.getString("date_of_first_cd4"),
                        resultSet.getString("tb_status_at_enrolment"),
                        resultSet.getString("arv_regimen_at_art_start"),
                        resultSet.getString("current_arv_regimen"),
                        resultSet.getString("art_start_date"),
                        resultSet.getString("oi_drugs_medication"),
                        resultSet.getString("arv_medication"),
                        resultSet.getString("anti_tb_medication"),
                        resultSet.getString("other_drugs_prescribed"),
                        resultSet.getString("prior_art"),
                        resultSet.getString("current_weight"),
                        resultSet.getString("current_weight_date"),
                        resultSet.getString("reasons_for_stopping_arvs"),
                        resultSet.getString("tpt_start_date"),
                        resultSet.getString("tpt_completion_date"),
                        resultSet.getString("current_tb_status"),
                        resultSet.getString("cd4_count_current"),
                        resultSet.getString("date_of_current_cd4"),
                        resultSet.getString("current_viral_Load"),
                        resultSet.getString("date_of_current_viral_load"),
                        resultSet.getString("tb_treatment_completed"),
                        resultSet.getString("current_height"),
                        resultSet.getString("current_temperature"),
                        resultSet.getString("current_systolic"),
                        resultSet.getString("current_diastolic"),
                        resultSet.getString("current_bmi"),
                        resultSet.getString("current_muac_child"),
                        resultSet.getString("current_muac_bmi"),
                        resultSet.getString("functional_status"),
                        resultSet.getString("current_who_hiv_stage")
                );
                records.add(record);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return records;
    }

}
