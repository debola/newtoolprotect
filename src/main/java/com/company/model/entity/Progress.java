package com.company.model.entity;


public class Progress {
  private String facility_name;
  private String datim_code;
  private String sex;
  private String dob;
  private String creator;
  private String date_created;
  private String enrolment_date;
  private String type_of_patient;
  private String previously_known_hiv_status;
  private String end_of_treatment;
  private String previously_known_hiv_status_presumtive;
  private String tested_for_hiv;
  private String type_of_care;
  private String hiv_test_result;
  private String treatment_regimen;
  private String date_registered;
  private String date_treatment_started;
  private String treatment_outcome_date;
  private String treatment_outcome;
  private String tb_screening_score;
  private String hiv_status;
  private String on_art;
  private String outcome;
  private String type_of_treatment_regimen;
  private String registration_group;
  private String art_start_date;
  private String outcome_date;
  private String form_id;

  public String getFacility_name() {
    return facility_name;
  }

  public void setFacility_name(String facility_name) {
    this.facility_name = facility_name;
  }

  public String getDatim_code() {
    return datim_code;
  }

  public void setDatim_code(String datim_code) {
    this.datim_code = datim_code;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }

  public String getDob() {
    return dob;
  }

  public void setDob(String dob) {
    this.dob = dob;
  }

  public String getCreator() {
    return creator;
  }

  public void setCreator(String creator) {
    this.creator = creator;
  }

  public String getDate_created() {
    return date_created;
  }

  public void setDate_created(String date_created) {
    this.date_created = date_created;
  }

  public String getEnrolment_date() {
    return enrolment_date;
  }

  public void setEnrolment_date(String enrolment_date) {
    this.enrolment_date = enrolment_date;
  }

  public String getType_of_patient() {
    return type_of_patient;
  }

  public void setType_of_patient(String type_of_patient) {
    this.type_of_patient = type_of_patient;
  }

  public String getPreviously_known_hiv_status() {
    return previously_known_hiv_status;
  }

  public void setPreviously_known_hiv_status(String previously_known_hiv_status) {
    this.previously_known_hiv_status = previously_known_hiv_status;
  }

  public String getEnd_of_treatment() {
    return end_of_treatment;
  }

  public void setEnd_of_treatment(String end_of_treatment) {
    this.end_of_treatment = end_of_treatment;
  }

  public String getPreviously_known_hiv_status_presumtive() {
    return previously_known_hiv_status_presumtive;
  }

  public void setPreviously_known_hiv_status_presumtive(String previously_known_hiv_status_presumtive) {
    this.previously_known_hiv_status_presumtive = previously_known_hiv_status_presumtive;
  }

  public String getTested_for_hiv() {
    return tested_for_hiv;
  }

  public void setTested_for_hiv(String tested_for_hiv) {
    this.tested_for_hiv = tested_for_hiv;
  }

  public String getType_of_care() {
    return type_of_care;
  }

  public void setType_of_care(String type_of_care) {
    this.type_of_care = type_of_care;
  }

  public String getHiv_test_result() {
    return hiv_test_result;
  }

  public void setHiv_test_result(String hiv_test_result) {
    this.hiv_test_result = hiv_test_result;
  }

  public String getTreatment_regimen() {
    return treatment_regimen;
  }

  public void setTreatment_regimen(String treatment_regimen) {
    this.treatment_regimen = treatment_regimen;
  }

  public String getDate_registered() {
    return date_registered;
  }

  public void setDate_registered(String date_registered) {
    this.date_registered = date_registered;
  }

  public String getDate_treatment_started() {
    return date_treatment_started;
  }

  public void setDate_treatment_started(String date_treatment_started) {
    this.date_treatment_started = date_treatment_started;
  }

  public String getTreatment_outcome_date() {
    return treatment_outcome_date;
  }

  public void setTreatment_outcome_date(String treatment_outcome_date) {
    this.treatment_outcome_date = treatment_outcome_date;
  }

  public String getTreatment_outcome() {
    return treatment_outcome;
  }

  public void setTreatment_outcome(String treatment_outcome) {
    this.treatment_outcome = treatment_outcome;
  }

  public String getTb_screening_score() {
    return tb_screening_score;
  }

  public void setTb_screening_score(String tb_screening_score) {
    this.tb_screening_score = tb_screening_score;
  }

  public String getHiv_status() {
    return hiv_status;
  }

  public void setHiv_status(String hiv_status) {
    this.hiv_status = hiv_status;
  }

  public String getOn_art() {
    return on_art;
  }

  public void setOn_art(String on_art) {
    this.on_art = on_art;
  }

  public String getOutcome() {
    return outcome;
  }

  public void setOutcome(String outcome) {
    this.outcome = outcome;
  }

  public String getType_of_treatment_regimen() {
    return type_of_treatment_regimen;
  }

  public void setType_of_treatment_regimen(String type_of_treatment_regimen) {
    this.type_of_treatment_regimen = type_of_treatment_regimen;
  }

  public String getRegistration_group() {
    return registration_group;
  }

  public void setRegistration_group(String registration_group) {
    this.registration_group = registration_group;
  }

  public String getArt_start_date() {
    return art_start_date;
  }

  public void setArt_start_date(String art_start_date) {
    this.art_start_date = art_start_date;
  }

  public String getOutcome_date() {
    return outcome_date;
  }

  public void setOutcome_date(String outcome_date) {
    this.outcome_date = outcome_date;
  }

  public String getForm_id() {
    return form_id;
  }

  public void setForm_id(String form_id) {
    this.form_id = form_id;
  }

  public Progress(
          String facility_name,
          String datim_code,
          String sex,
          String dob,
          String creator,
          String date_created,
          String enrolment_date,
          String type_of_patient,
          String previously_known_hiv_status,
          String end_of_treatment,
          String previously_known_hiv_status_presumtive,
          String tested_for_hiv,
          String type_of_care,
          String hiv_test_result,
          String treatment_regimen,
          String date_registered,
          String date_treatment_started,
          String treatment_outcome_date,
          String treatment_outcome,
          String tb_screening_score,
          String hiv_status,
          String on_art,
          String outcome,
          String type_of_treatment_regimen,
          String registration_group,
          String art_start_date,
          String outcome_date,
          String form_id) {

    this.facility_name = facility_name;
    this.datim_code = datim_code;
    this.sex = sex;
    this.dob = dob;
    this.enrolment_date = enrolment_date;
    this.creator = creator;
    this.date_created = date_created;
    this.type_of_patient = type_of_patient;
    this.previously_known_hiv_status = previously_known_hiv_status;
    this.end_of_treatment = end_of_treatment;
    this.previously_known_hiv_status_presumtive = previously_known_hiv_status_presumtive;
    this.tested_for_hiv = tested_for_hiv;
    this.type_of_care = type_of_care;
    this.art_start_date = art_start_date;
    this.date_treatment_started = date_treatment_started;
    this.hiv_test_result = hiv_test_result;
    this.treatment_regimen = treatment_regimen;
    this.date_registered= date_registered;
    this.treatment_outcome_date = treatment_outcome_date;
    this.treatment_outcome = treatment_outcome;
    this.tb_screening_score = tb_screening_score;
    this.hiv_status = hiv_status;
    this.on_art = on_art;
    this.outcome = outcome;
    this.type_of_treatment_regimen = type_of_treatment_regimen;
    this.registration_group = registration_group;
    this.outcome_date = outcome_date;
    this.form_id = form_id;
  }

}
