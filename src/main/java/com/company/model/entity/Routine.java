package com.company.model.entity;


public class Routine {
  private String facility_name;
  private String datim_code;
  private String sex;
  private String dob;
  private String enrolment_date;
  private String weight_at_enrolment;
  private String who_stage_at_enrolment;
  private String symptoms;
  private String cd4_count_at_hiv_diagnosis;
  private String date_of_first_cd4;
  private String tb_status_at_enrolment;
  private String arv_regimen_at_art_start;
  private String current_arv_regimen;
  private String art_start_date;
  private String oi_drugs_medication;
  private String arv_medication;
  private String anti_tb_medication;
  private String other_drugs_prescribed;
  private String prior_art;
  private String current_weight;
  private String current_weight_date;
  private String reasons_for_stopping_arvs;
  private String tpt_start_date;
  private String tpt_completion_date;
  private String current_tb_status;
  private String cd4_count_current;
  private String date_of_current_cd4;
  private String current_viral_Load;
  private String date_of_current_viral_load;
  private String tb_treatment_completed;
  private String current_height;
  private String current_temperature;
  private String current_systolic;
  private String current_diastolic;
  private String current_bmi;
  private String current_muac_child;
  private String current_muac_bmi;
  private String functional_status;
  private String current_who_hiv_stage;

  public String getFacility_name() {
    return facility_name;
  }

  public void setFacility_name(String facility_name) {
    this.facility_name = facility_name;
  }

  public String getDatim_code() {
    return datim_code;
  }

  public void setDatim_code(String datim_code) {
    this.datim_code = datim_code;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }

  public String getDob() {
    return dob;
  }

  public void setDob(String dob) {
    this.dob = dob;
  }

  public String getEnrolment_date() {
    return enrolment_date;
  }

  public void setEnrolment_date(String enrolment_date) {
    this.enrolment_date = enrolment_date;
  }

  public String getWeight_at_enrolment() {
    return weight_at_enrolment;
  }

  public void setWeight_at_enrolment(String weight_at_enrolment) {
    this.weight_at_enrolment = weight_at_enrolment;
  }

  public String getWho_stage_at_enrolment() {
    return who_stage_at_enrolment;
  }

  public void setWho_stage_at_enrolment(String who_stage_at_enrolment) {
    this.who_stage_at_enrolment = who_stage_at_enrolment;
  }

  public String getSymptoms() {
    return symptoms;
  }

  public void setSymptoms(String symptoms) {
    this.symptoms = symptoms;
  }

  public String getCd4_count_at_hiv_diagnosis() {
    return cd4_count_at_hiv_diagnosis;
  }

  public void setCd4_count_at_hiv_diagnosis(String cd4_count_at_hiv_diagnosis) {
    this.cd4_count_at_hiv_diagnosis = cd4_count_at_hiv_diagnosis;
  }

  public String getDate_of_first_cd4() {
    return date_of_first_cd4;
  }

  public void setDate_of_first_cd4(String date_of_first_cd4) {
    this.date_of_first_cd4 = date_of_first_cd4;
  }

  public String getTb_status_at_enrolment() {
    return tb_status_at_enrolment;
  }

  public void setTb_status_at_enrolment(String tb_status_at_enrolment) {
    this.tb_status_at_enrolment = tb_status_at_enrolment;
  }

  public String getArv_regimen_at_art_start() {
    return arv_regimen_at_art_start;
  }

  public void setArv_regimen_at_art_start(String arv_regimen_at_art_start) {
    this.arv_regimen_at_art_start = arv_regimen_at_art_start;
  }

  public String getCurrent_arv_regimen() {
    return current_arv_regimen;
  }

  public void setCurrent_arv_regimen(String current_arv_regimen) {
    this.current_arv_regimen = current_arv_regimen;
  }

  public String getArt_start_date() {
    return art_start_date;
  }

  public void setArt_start_date(String art_start_date) {
    this.art_start_date = art_start_date;
  }

  public String getOi_drugs_medication() {
    return oi_drugs_medication;
  }

  public void setOi_drugs_medication(String oi_drugs_medication) {
    this.oi_drugs_medication = oi_drugs_medication;
  }

  public String getArv_medication() {
    return arv_medication;
  }

  public void setArv_medication(String arv_medication) {
    this.arv_medication = arv_medication;
  }

  public String getAnti_tb_medication() {
    return anti_tb_medication;
  }

  public void setAnti_tb_medication(String anti_tb_medication) {
    this.anti_tb_medication = anti_tb_medication;
  }

  public String getOther_drugs_prescribed() {
    return other_drugs_prescribed;
  }

  public void setOther_drugs_prescribed(String other_drugs_prescribed) {
    this.other_drugs_prescribed = other_drugs_prescribed;
  }

  public String getPrior_art() {
    return prior_art;
  }

  public void setPrior_art(String prior_art) {
    this.prior_art = prior_art;
  }

  public String getCurrent_weight() {
    return current_weight;
  }

  public void setCurrent_weight(String current_weight) {
    this.current_weight = current_weight;
  }

  public String getCurrent_weight_date() {
    return current_weight_date;
  }

  public void setCurrent_weight_date(String current_weight_date) {
    this.current_weight_date = current_weight_date;
  }

  public String getReasons_for_stopping_arvs() {
    return reasons_for_stopping_arvs;
  }

  public void setReasons_for_stopping_arvs(String reasons_for_stopping_arvs) {
    this.reasons_for_stopping_arvs = reasons_for_stopping_arvs;
  }

  public String getTpt_start_date() {
    return tpt_start_date;
  }

  public void setTpt_start_date(String tpt_start_date) {
    this.tpt_start_date = tpt_start_date;
  }

  public String getTpt_completion_date() {
    return tpt_completion_date;
  }

  public void setTpt_completion_date(String tpt_completion_date) {
    this.tpt_completion_date = tpt_completion_date;
  }

  public String getCurrent_tb_status() {
    return current_tb_status;
  }

  public void setCurrent_tb_status(String current_tb_status) {
    this.current_tb_status = current_tb_status;
  }

  public String getCd4_count_current() {
    return cd4_count_current;
  }

  public void setCd4_count_current(String cd4_count_current) {
    this.cd4_count_current = cd4_count_current;
  }

  public String getDate_of_current_cd4() {
    return date_of_current_cd4;
  }

  public void setDate_of_current_cd4(String date_of_current_cd4) {
    this.date_of_current_cd4 = date_of_current_cd4;
  }

  public String getCurrent_viral_Load() {
    return current_viral_Load;
  }

  public void setCurrent_viral_Load(String current_viral_Load) {
    this.current_viral_Load = current_viral_Load;
  }

  public String getDate_of_current_viral_load() {
    return date_of_current_viral_load;
  }

  public void setDate_of_current_viral_load(String date_of_current_viral_load) {
    this.date_of_current_viral_load = date_of_current_viral_load;
  }

  public String getTb_treatment_completed() {
    return tb_treatment_completed;
  }

  public void setTb_treatment_completed(String tb_treatment_completed) {
    this.tb_treatment_completed = tb_treatment_completed;
  }

  public String getCurrent_height() {
    return current_height;
  }

  public void setCurrent_height(String current_height) {
    this.current_height = current_height;
  }

  public String getCurrent_temperature() {
    return current_temperature;
  }

  public void setCurrent_temperature(String current_temperature) {
    this.current_temperature = current_temperature;
  }

  public String getCurrent_systolic() {
    return current_systolic;
  }

  public void setCurrent_systolic(String current_systolic) {
    this.current_systolic = current_systolic;
  }

  public String getCurrent_diastolic() {
    return current_diastolic;
  }

  public void setCurrent_diastolic(String current_diastolic) {
    this.current_diastolic = current_diastolic;
  }

  public String getCurrent_bmi() {
    return current_bmi;
  }

  public void setCurrent_bmi(String current_bmi) {
    this.current_bmi = current_bmi;
  }

  public String getCurrent_muac_child() {
    return current_muac_child;
  }

  public void setCurrent_muac_child(String current_muac_child) {
    this.current_muac_child = current_muac_child;
  }

  public String getCurrent_muac_bmi() {
    return current_muac_bmi;
  }

  public void setCurrent_muac_bmi(String current_muac_bmi) {
    this.current_muac_bmi = current_muac_bmi;
  }

  public String getFunctional_status() {
    return functional_status;
  }

  public void setFunctional_status(String functional_status) {
    this.functional_status = functional_status;
  }

  public String getCurrent_who_hiv_stage() {
    return current_who_hiv_stage;
  }

  public void setCurrent_who_hiv_stage(String current_who_hiv_stage) {
    this.current_who_hiv_stage = current_who_hiv_stage;
  }

//  @Override
//  public String toString() {
//    return "\n\nRecords: "
//            + "facility_name: " + facility_name + ", "
//            + "datim_code: " + datim_code + ", "
//            + "sex: " + sex + ", "
//            + "enrolment_date: " + enrolment_date + " ";
//  }

  public Routine(
          String facility_name,
          String datim_code,
          String sex,
          String dob,
          String enrolment_date,
          String weight_at_enrolment,
          String who_stage_at_enrolment,
          String symptoms,
          String cd4_count_at_hiv_diagnosis,
          String date_of_first_cd4,
          String tb_status_at_enrolment,
          String arv_regimen_at_art_start,
          String current_arv_regimen,
          String art_start_date,
          String oi_drugs_medication,
          String arv_medication,
          String anti_tb_medication,
          String other_drugs_prescribed,
          String prior_art,
          String current_weight,
          String current_weight_date,
          String reasons_for_stopping_arvs,
          String tpt_start_date,
          String tpt_completion_date,
          String current_tb_status,
          String cd4_count_current,
          String date_of_current_cd4,
          String current_viral_Load,
          String date_of_current_viral_load,
          String tb_treatment_completed,
          String current_height,
          String current_temperature,
          String current_systolic,
          String current_diastolic,
          String current_bmi,
          String current_muac_child,
          String current_muac_bmi,
          String functional_status,
          String current_who_hiv_stage) {

    this.facility_name = facility_name;
    this.datim_code = datim_code;
    this.sex = sex;
    this.dob = dob;
    this.enrolment_date = enrolment_date;
    this.weight_at_enrolment = weight_at_enrolment;
    this.who_stage_at_enrolment = who_stage_at_enrolment;
    this.symptoms = symptoms;
    this.cd4_count_at_hiv_diagnosis = cd4_count_at_hiv_diagnosis;
    this.date_of_first_cd4 = date_of_first_cd4;
    this.tb_status_at_enrolment = tb_status_at_enrolment;
    this.arv_regimen_at_art_start = arv_regimen_at_art_start;
    this.current_arv_regimen = current_arv_regimen;
    this.art_start_date = art_start_date;
    this.oi_drugs_medication = oi_drugs_medication;
    this.arv_medication = arv_medication;
    this.arv_medication= arv_medication;
    this.other_drugs_prescribed = other_drugs_prescribed;
    this.prior_art = prior_art;
    this.current_weight = current_weight;
    this.current_weight_date = current_weight_date;
    this.reasons_for_stopping_arvs = reasons_for_stopping_arvs;
    this.tpt_start_date = tpt_start_date;
    this.tpt_completion_date = tpt_completion_date;
    this.current_tb_status = current_tb_status;
    this.cd4_count_current = cd4_count_current;
    this.date_of_current_cd4 = date_of_current_cd4;
    this.current_viral_Load = current_viral_Load;
    this.date_of_current_viral_load = date_of_current_viral_load;
    this.tb_treatment_completed = tb_treatment_completed;
    this.current_height = current_height;
    this.current_temperature = current_temperature;
    this.current_systolic = current_systolic;
    this.current_diastolic = current_diastolic;
    this.current_bmi = current_bmi;
    this.current_muac_child = current_muac_child;
    this.current_muac_bmi = current_muac_bmi;
    this.functional_status = functional_status;
    this.current_who_hiv_stage = current_who_hiv_stage;
    this.anti_tb_medication = anti_tb_medication;
  }

}
