package com.company.view;

import com.company.PostRequestExt;
import com.company.SqliteConnect;
import com.company.controller.ProgressController;
import com.company.controller.RoutineController;
import com.company.model.entity.Progress;
import com.company.model.entity.Routine;

import java.awt.print.Printable;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;
import java.util.*;
import java.util.List;

public class View {
    private final RoutineController routineController = new RoutineController();
    private final ProgressController progressController = new ProgressController();
    private final Map<String, Printable> menu = new LinkedHashMap<String, Printable>();

    public View() throws SQLException, IOException, InterruptedException {
        System.out.println("Shoot your shot:\n");
        this.createDB();
        this.getAllRecords();
        this.getAllProgressRecords();
    }

    private boolean netIsAvailable() {
        try {
            final URL url = new URL("http://www.google.com");
            final URLConnection conn = url.openConnection();
            conn.connect();
            conn.getInputStream().close();
            return true;
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            return false;
        }
    }
    private void getAllRecords() throws SQLException, IOException, InterruptedException {
        List<Routine> records = new ArrayList<>();
        System.out.println("\nFetching records...");
        records = new SqliteConnect().selectAll();
        if(records.isEmpty()){
            records = routineController.findAll();
            new SqliteConnect().delete(records.get(0).getDatim_code());
            new SqliteConnect().insert(records);
        }
        if(this.netIsAvailable()){
            PostRequestExt postRequestExt = new PostRequestExt();
            postRequestExt.post(records);

        }
        System.out.println(records);
    }
    private void getAllProgressRecords() throws SQLException, IOException, InterruptedException {
        List<Progress> recordProgress = new ArrayList<>();
        System.out.println("\nFetching records...");

        recordProgress = new SqliteConnect().selectAllProgress();
        if(recordProgress.isEmpty()){
            recordProgress = progressController.findAll();
            System.out.println("\nSaving records...");
            new SqliteConnect().deleteProgress(recordProgress.get(0).getDatim_code());
            new SqliteConnect().insertProgress(recordProgress);
        }
        if(this.netIsAvailable()){
            System.out.println("\nPosting records...");
            PostRequestExt postRequestExt = new PostRequestExt();
            postRequestExt.postProgress(recordProgress);
        }
    }
    private void createDB() throws SQLException {
        File file = new File("C:/sqlite/db/protect");
        if(!file.exists()) //here's how to check
        {
            System.out.println("\nCreating database...");
            SqliteConnect sqliteConnect = new SqliteConnect();
            sqliteConnect.createNewDatabase("protect");
            sqliteConnect.createNewTable();
        }
    }

}
